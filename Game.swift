
//  Game.swift  Copyright (c) Kari Laitinen

//  http://www.naturalprogramming.com

//  2014-09-22  File created.
//  2015-10-16  Converted to Swift 2.

/*  This is a simple Swift program that can be run in a Terminal window
    in the OS X operating system. A recent version of Xcode
    must be installed in order to run this program.
    
    After changing to the right directory in Terminal,
    compile and run this program with the command

         xcrun swift Game.swift
*/

import Foundation

// Here begins The main program.

// The print() function automatically prints a newline
// character after the text. If you want to disable this
// feature you must provide terminator:"" as a parameter.

print( "\n This program is a computer game."
    +  "\n Please, type in an integer : ", terminator:"" )

// The next statement reads a line from keyboard, and converts
// the string of characters to an Int value.

var given_integer = Int( readLine()! )!

var larger_integer = given_integer + 1

print( "\n You typed in \(given_integer). " )
print( " My number is \(larger_integer). " )
print( " Sorry, you lost. I won. The game is over.\n\n" )


