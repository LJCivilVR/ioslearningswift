
//  Reverse.swift  Copyright (c) Kari Laitinen

//  http://www.naturalprogramming.com

//  2014-09-29  File created.
//  2015-10-17  Last modification.

import Foundation
 
var  array_of_integers = [ Int ] ()
var  integer_index     =  0
var  integer_from_keyboard : Int! 

print( "\n This program reads integers from the keyboard."
    +  "\n After receiving a zero, it prints the numbers"
    +  "\n in reverse order. Please, start entering numbers."
    +  "\n The program will stop when you enter a zero.\n" )

repeat
{
   print( " "  +  String( integer_index ) +  "  Enter an integer: ",
          terminator: "" )

   integer_from_keyboard  =  Int( readLine()! )

   array_of_integers.append( integer_from_keyboard )
   integer_index  +=  1
}
 while  integer_from_keyboard  !=  0 

print( "\n Reverse order:  \n" )

while  integer_index  >  0 
{
   integer_index  -=  1
   print( "   " + String( array_of_integers[ integer_index ] ),
          terminator: "" )
}

print( "\n\n" )
