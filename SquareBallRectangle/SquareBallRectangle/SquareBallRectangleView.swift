
//  SquareBallRectangleView.swift Copyright (c) 2015 Kari Laitinen.

//  2015-08-18  File created.
//  2015-10-17  Modifications for Xcode 7, Swift 2
//  2017-08-15  Last modification.


/*  This app demonstrates the use of UISegmentedControl and the drawing
    of some basic shapes.

    It is important that 'Value changed' is selected as the 
    triggering action in the UISegmentedControl. By selecting
    'Tap Up Inside' the program did not work.

*/

import UIKit

class SquareBallRectangleView: UIView
{
   @IBOutlet var shape_selection_buttons : UISegmentedControl!

   @IBAction func shape_selection_was_made( _ sender : AnyObject )
   {
      setNeedsDisplay()  // "repaint" the view
   }
   
    var edge_line_width : CGFloat = 2
    
    @IBAction func ChangeEdge(_ sender: Any) {
        
        edge_line_width = edge_line_width + 1
        
        
        if edge_line_width == 10 {
            edge_line_width = 2
        }
        
    }
    
    
    
   override func draw( _ rect: CGRect )
   {
    
    
      let context: CGContext = UIGraphicsGetCurrentContext()!
         
      context.setLineWidth(edge_line_width)
      context.setStrokeColor(UIColor.blue.cgColor )
      
      let view_center_point_x = self.bounds.size.width / 2
      let view_center_point_y = self.bounds.size.height / 2
      
      
      if  shape_selection_buttons.selectedSegmentIndex == 0
      {
         // This means that a square must be drawn
         context.beginPath()
         context.setFillColor(UIColor.cyan.cgColor )

         let centered_square = CGRect( x: view_center_point_x - 80,
            y: view_center_point_y - 80, width: 160, height: 160 )
         context.addRect(centered_square )
         context.strokePath()
        
         context.fill(centered_square )
        
         // Above, CGContextFillPath did not work after CGContextStrokePath was used.
         // Therefore, I used the CGContextFillRect method.
         
      }
      else if  shape_selection_buttons.selectedSegmentIndex == 1
      {
         // We'll draw a ball
         context.beginPath()
         context.setFillColor(UIColor.magenta.cgColor )

         let centered_square = CGRect( x: view_center_point_x - 80,
            y: view_center_point_y - 80, width: 160, height: 160 )
         context.addEllipse(in: centered_square )
         context.strokePath()
       
         context.fillEllipse(in: centered_square )
         
      }
      else if  shape_selection_buttons.selectedSegmentIndex == 2 
      {
//        Create a triangle
        context.beginPath()
        context.setFillColor(UIColor.yellow.cgColor )
        
        context.move(to: CGPoint(x: view_center_point_x, y: view_center_point_y - 80))
        context.addLine(to: CGPoint(x: view_center_point_x + 96, y: view_center_point_y + 80))
        context.addLine(to: CGPoint(x: view_center_point_x - 96, y: view_center_point_y + 80))
        context.addLine(to: CGPoint(x: view_center_point_x, y: view_center_point_y - 80))
        
        
        context.drawPath(using: CGPathDrawingMode.fillStroke)
        
        
         // This means that a rectangle will be drawn
//         context.beginPath()
//         context.setFillColor(UIColor.yellow.cgColor )
//
//         let centered_rectangle = CGRect( x: view_center_point_x - 120,
//            y: view_center_point_y - 60, width: 240, height: 120 )
//         context.addRect(centered_rectangle )
//         context.strokePath()
//         context.fill(centered_rectangle )
        
      }
      else if shape_selection_buttons.selectedSegmentIndex == 3 {
        
        // Next we'll draw the 'missing pie' of the pacman.
        // The center point is now 80 points to the right of the center point
        // of the pacman. When the last parameter for the CGContextAddArc
        // function is 1, we'll make a clockwise arc.
        
//        Create a Pie Slice.
        context.beginPath()
        context.move( to: CGPoint( x: view_center_point_x ,
                                   y: view_center_point_y - 70) ) // start from center
        context.addArc( center: CGPoint( x: view_center_point_x ,
                                         y: view_center_point_y - 70),
                        radius: 140,
                        startAngle: CGFloat( 0.75 * Double.pi ),
                        endAngle: CGFloat( 2.25 * Double.pi ),
                        clockwise: true )
        context.setFillColor(UIColor.red.cgColor )
        context.closePath()
        context.drawPath( using: CGPathDrawingMode.fillStroke )
    
   }

}
}

/*
            // With the following statements you can create a triangle.
            // These can be useful when you do the exercises.
 
            context.beginPath()
            context.setFillColor(UIColor.yellow.cgColor )
             
            context.move(to: CGPoint(x: view_center_point_x, y: view_center_point_y - 80))
            context.addLine(to: CGPoint(x: view_center_point_x + 96, y: view_center_point_y + 80))
            context.addLine(to: CGPoint(x: view_center_point_x - 96, y: view_center_point_y + 80))
            context.addLine(to: CGPoint(x: view_center_point_x, y: view_center_point_y - 80))
            
            // The following call both fills and draws the path.
            
            context.drawPath(using: CGPathDrawingMode.fillStroke)

*/


