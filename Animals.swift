
//  Animals.swift  Copyright (c) Kari Laitinen

//  http://www.naturalprogramming.com

//  2014-10-02  File created.
//  2016-11-07  Last modification.

/* This program provides a simple example of a class.

   - initializers, i.e., consturctors are written with the keyword init
   - if no external name is provided for initializer parameters, the local
     name is also the external name
   - by writing an underscore _ , you can have an initializer for which no
     external name needs to be written
*/

import Foundation

class  Animal
{
   var species_name : String
   var stomach_contents : String
    var name_animal : String
    

    init( _ given_species_name : String , _ animalsnaam : String)
   {
    
        species_name      =  given_species_name
        name_animal = animalsnaam
        stomach_contents  =  ""
   
    
   }
    
    
   init( _ another_animal : Animal )
   {
      species_name      =  another_animal.species_name
        name_animal = another_animal.name_animal
      stomach_contents  =  another_animal.stomach_contents
   }
    
    init() {
        species_name      =  "default species"
        name_animal = "nameless"
        stomach_contents  =  ""
    }
    

   func feed( _ food_for_this_animal : String )
   {
      stomach_contents  =
          stomach_contents  +  food_for_this_animal  +  ", "
   }
    
   func make_speak()
   {
    
    if (stomach_contents != "" ) {
      print( "\n Hello, I am a " + species_name     + "and my name is: \(name_animal)" + "."
           + "\n I have eaten: " + stomach_contents )
    }
    else {
        print( "\n Hello, I am a " + species_name     + "and my name is: \(name_animal)" + "."
            + "\n My stomach is empty." )
    }
}
    
    func make_stomach_empty() {
        stomach_contents  =  ""
        
        
        
    }
}

var cat_object  =  Animal( "cat" , "Arnold")
var dog_object  =  Animal( "vegetarian dog", "" )
var default_animal = Animal()




cat_object.feed( "fish" )
cat_object.feed( "chicken" )

dog_object.feed( "tomatoes" )
dog_object.feed( "potatoes" )



var another_cat  =  Animal( cat_object )

another_cat.feed( "milk" )

default_animal.make_speak()
cat_object.make_speak()
dog_object.make_speak()
another_cat.make_speak()


print( "\n" )

default_animal.make_stomach_empty()
cat_object.make_stomach_empty()
dog_object.make_stomach_empty()

default_animal.make_speak()
cat_object.make_speak()
dog_object.make_speak()
another_cat.make_speak()

print( "\n" )




